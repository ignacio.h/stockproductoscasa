import json
from flask import jsonify, request, Response
from config.settings import app, db
from entities.producto import producto as p


@app.route('/')
def hello_world():
    return "hello world!"

@app.route('/producto/add_producto/', methods=['POST'])
def add_producto():
    body = request.get_json()
    if validar_body_prodcuto(body):
        if p.insertar_producto(body["nombre"], body["cantidad"], body["cantidad_referencia"]):
            return Response("", status=204)
        return Response("No se ha podido dar de alta el nuevo producto.", status=400)
    return Response("No se han introducido los valores necesarios para dar de alta el nuevo producto.", status=400)

@app.route('/producto/get_productos/', methods=['GET'])
def get_productos():
    productos = p.buscar_todos_productos()
    return jsonify({'productos': productos})

@app.route('/producto/get_producto/<nombre>', methods=['GET'])
def get_producto(nombre):
    prod = p.buscar_un_producto(nombre)
    if prod is None or prod == 'None':
        return Response("No existe el producto '{0}'".format(nombre), status=400)
    return jsonify(prod)

@app.route('/prodcuto/set_stock/', methods=['PUT'])
def set_stock():
    body = request.get_json()
    if "nombre" in body and body["nombre"] is None or body["nombre"].strip() == "":
        return Response("Nombre del producto incorrecto.", status=400)
    elif "cantidad" in body and body["cantidad"] < 0:
        return Response("La cantidad debe ser mayor o igual que 0.", status=400)
    else:
        if p.establecer_cantidad(body["nombre"], body["cantidad"]):
            return Response("", status=204)
    return Response("No se ha podido establecer la cantidad para el producto '{0}'".format(body["nombre"]), status=400)

@app.route('/producto/eliminar_producto/', methods=['DELETE'])
def eliminar_producto():
    body = request.get_json()
    if "nombre" in body and body["nombre"] is None or body["nombre"].strip() == "":
        return Response("Nombre del producto incorrecto.", status=400)
    if p.eliminar_producto(body["nombre"]):
        return Response("", status=204)
    return Response("No se ha podido eliminar el producto '{0}'".format(body["nombre"]), status=400)

@app.route('/producto/compra/', methods=['GET'])
def compra():
    productos = p.buscar_productos_compra()
    return jsonify({"productos": productos})

def validar_body_prodcuto(body):
    return "nombre" in body and "cantidad" in body and "cantidad_referencia" in body


app.run(host="0.0.0.0", port=5000)
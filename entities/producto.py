import json
from config.settings import db

class producto(db.Model):
    __tablename__ = "producto"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(100),  unique=True, nullable=False)
    cantidad = db.Column(db.Integer, nullable=False, default=0)
    cantidad_referencia = db.Column(db.Integer, nullable=False)

    @staticmethod
    def insertar_producto(_nombre, _cantidad, _referencia):
        try:
            db.session.add(producto(nombre = _nombre, cantidad = _cantidad, cantidad_referencia = _referencia))
            db.session.commit()
            return True
        except:
            return False

    @staticmethod
    def buscar_un_producto(_nombre):
        return producto.query.filter_by(nombre=_nombre).first().__repr__()

    @staticmethod
    def buscar_por_cantidad(_cantidad):
        return [pr.__repr__() for pr in producto.query.filter_by(cantidad=_cantidad).all()]
    
    @staticmethod
    def buscar_todos_productos():
        return [pr.__repr__() for pr in producto.query.all()]

    @staticmethod
    def eliminar_producto(_nombre):
        try:
            producto.query.filter_by(nombre=_nombre).delete()
            db.session.commit()
            return True
        except:
            return False

    @staticmethod
    def establecer_cantidad(_nombre, _cantidad):
        prod = producto.query.filter_by(nombre=_nombre).first()
        if prod is not None:
            prod.cantidad = _cantidad
            db.session.commit()
            return True
        return False

    @staticmethod
    def buscar_productos_compra():
        return [pc.__repr__() for pc in producto.query.filter(producto.cantidad <= producto.cantidad_referencia).all()]
        

    def __repr__(self):
        prod = {
            'nombre': self.nombre,
            'cantidad': self.cantidad,
            'cantidad_referencia': self.cantidad_referencia,
        }
        return prod

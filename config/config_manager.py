from configparser import ConfigParser, ExtendedInterpolation
from pathlib import Path
import os.path

class config_manager():
    __ruta_ini = os.path.join(Path(__file__).parent, 'config_variables.ini')
    __config = None


    @classmethod
    def __get_config(cls):
        if cls.__config == None:
            cls.__config = ConfigParser(interpolation=ExtendedInterpolation())
            cls.__config.read_file(open(cls.__ruta_ini))


    @classmethod
    def get_valor_configuracion(cls, section, option):
        if cls.__config == None:
            cls.__get_config()
        if not cls.__config.has_section(section):
            raise ValueError("La seccion {0} no existe".format(section))
        if not cls.__config.has_option(section, option):
            raise ValueError("La opcion {0} no existe dentro de la seccion {1}".format(option, section))

        return cls.__config.get(section, option)


    @classmethod
    def get_ruta_bd(cls):
        if cls.__config == None:
            cls.__get_config()
        try:
            ruta_home = Path(__file__).parent.parent
            carpeta_bd = cls.get_valor_configuracion("BD", "ruta")
            fichero_bd = cls.get_valor_configuracion("BD", "nombre")
            return os.path.join(ruta_home, carpeta_bd, fichero_bd)
        except:
            raise ValueError("No se puede obtener la ruta de la bd.")

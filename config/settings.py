from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from config.config_manager import config_manager as cm


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{0}'.format(cm.get_ruta_bd())
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
